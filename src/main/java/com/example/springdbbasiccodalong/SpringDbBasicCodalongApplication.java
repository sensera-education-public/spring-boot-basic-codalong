package com.example.springdbbasiccodalong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDbBasicCodalongApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDbBasicCodalongApplication.class, args);
    }

}
