package com.example.springdbbasiccodalong;

import com.example.springdbbasiccodalong.domain.Address;
import com.example.springdbbasiccodalong.domain.Car;
import com.example.springdbbasiccodalong.domain.Person;
import com.example.springdbbasiccodalong.exceptions.NotFoundException;
import com.example.springdbbasiccodalong.exceptions.PersonHasCarException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
@AllArgsConstructor
public class PersonController {

    PersonService personService;

    @GetMapping
    public List<PersonDTO> persons() {
        return personService.all()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public ResponseEntity<PersonDTO> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(
                    toDTO(personService.get(id)));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public PersonDTO create(@RequestBody CreatePerson createPerson) {
        return toDTO(
                personService.create(createPerson.getName()));
    }

    @PostMapping("{id}/addCar")
    public ResponseEntity<PersonDTO> addCar(@PathVariable("id") String id,
                                            @RequestBody AddCar addCar) {
        try {
            return ResponseEntity.ok(toDTO(
                    personService.addCar(id, addCar.getName())));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (PersonHasCarException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("{id}/addAddress")
    public ResponseEntity<PersonDTO> addAddress(@PathVariable("id") String id,
                                            @RequestBody AddAddress addAddress) {
        try {
            return ResponseEntity.ok(toDTO(
                    personService.addAddress(id, addAddress.getName())));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("{id}/addOccupation")
    public ResponseEntity<PersonDTO> addOccupation(@PathVariable("id") String id,
                                                @RequestBody AddOccupation addOccupation) {
        try {
            return ResponseEntity.ok(toDTO(
                    personService.addOccupation(id, addOccupation.getName())));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable("id") String id) {
        try {
            personService.delete(id);
            return ResponseEntity.ok().build();
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private PersonDTO toDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.car()
                        .map(Car::getName)
                        .orElse(""),
                person.addresses()
                        .map(Address::getName)
                        .toArray(String[]::new)
        );
    }

    @Value
    public static class PersonDTO {
        String id;
        String name;
        String carName;
        String[] addresses;
    }

    @Value
    public static class CreatePerson {
        String name;

        @JsonCreator
        public CreatePerson(
                @JsonProperty("name") String name) {
            this.name = name;
        }
    }

    @Value
    public static class AddCar {
        String name;

        @JsonCreator
        public AddCar(
                @JsonProperty("name") String name) {
            this.name = name;
        }
    }

    @Value
    public static class AddAddress {
        String name;

        @JsonCreator
        public AddAddress(
                @JsonProperty("name") String name) {
            this.name = name;
        }
    }

    @Value
    public static class AddOccupation {
        String name;

        @JsonCreator
        public AddOccupation(
                @JsonProperty("name") String name) {
            this.name = name;
        }
    }
}
