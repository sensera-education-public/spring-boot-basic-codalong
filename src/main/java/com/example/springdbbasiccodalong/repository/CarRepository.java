package com.example.springdbbasiccodalong.repository;

import com.example.springdbbasiccodalong.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car,String> {
}
