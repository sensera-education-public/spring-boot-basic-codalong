package com.example.springdbbasiccodalong.repository;

import com.example.springdbbasiccodalong.domain.Occupation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface OccupationRepository extends JpaRepository<Occupation,String> {
    Stream<Occupation> findByName(String name);
}
