package com.example.springdbbasiccodalong.repository;

import com.example.springdbbasiccodalong.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address,String> {
}
