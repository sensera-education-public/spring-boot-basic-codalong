package com.example.springdbbasiccodalong;

import com.example.springdbbasiccodalong.domain.Address;
import com.example.springdbbasiccodalong.domain.Car;
import com.example.springdbbasiccodalong.domain.Occupation;
import com.example.springdbbasiccodalong.domain.Person;
import com.example.springdbbasiccodalong.exceptions.NotFoundException;
import com.example.springdbbasiccodalong.exceptions.PersonHasCarException;
import com.example.springdbbasiccodalong.repository.OccupationRepository;
import com.example.springdbbasiccodalong.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PersonService {

    PersonRepository personRepository;
    OccupationRepository occupationRepository;

    public Stream<Person> all() {
        return personRepository.findAll().stream();
    }

    public Person get(String id) throws NotFoundException {
        return personRepository.findById(id)
                .orElseThrow(()-> new NotFoundException("Unable to find person with id "+id));
    }

    public Person create(String name) {
        Person person = new Person(name);
        return personRepository.save(person);
    }

    public void delete(String id) throws NotFoundException {
        personRepository.delete(get(id));
    }

    public Person addCar(String id, String carName) throws NotFoundException, PersonHasCarException {
        Person person = get(id);
        if (person.car().isPresent())
            throw new PersonHasCarException();
        Car car = new Car(carName, person);
        person.setCar(car);
        return personRepository.save(person);
    }

    public Person addAddress(String id, String addressName) throws NotFoundException {
        Person person = get(id);
        Address address = new Address(addressName, person);
        person.addAddress(address);
        return personRepository.save(person);
    }

    public Person addOccupation(String id, String name) throws NotFoundException {
        Person person = get(id);
        Occupation occupation = occupationRepository.findByName(name)
                .findAny()
                .orElse(new Occupation(name));
        if (person.hasOccupation(occupation))
            return person;
        occupation.addPerson(person);
        person.addOccupation(occupation);
        return personRepository.save(person);
    }
}
