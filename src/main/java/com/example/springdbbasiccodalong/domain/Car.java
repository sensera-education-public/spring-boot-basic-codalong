package com.example.springdbbasiccodalong.domain;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
public class Car {
    @Id String id;

    String name;

    @OneToOne(mappedBy = "car", optional = false)
    Person person;

    public Car(String name, Person person) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.person = person;
    }

    protected Car() { }  // Hibernate

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Car bil = (Car) o;
        return id != null && Objects.equals(id, bil.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
