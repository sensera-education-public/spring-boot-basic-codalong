package com.example.springdbbasiccodalong.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Entity
@Table(name = "person")
@Getter @Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Person {
    @Id
    String id;

    @Column(length = 100)
    String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    Car car;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    List<Address> addresses;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "person_occupation",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "occupation_id"))
    List<Occupation> occupations;

    public Person(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.addresses = new ArrayList<>();
        this.occupations = new ArrayList<>();
    }

    public Optional<Car> car() {
        return Optional.ofNullable(car);
    }

    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    public Stream<Address> addresses() {
        return addresses.stream();
    }

    public void addOccupation(Occupation occupation) {
        this.occupations.add(occupation);
    }

    public boolean hasOccupation(Occupation occupation) {
        return occupations.contains(occupation);
    }
}
