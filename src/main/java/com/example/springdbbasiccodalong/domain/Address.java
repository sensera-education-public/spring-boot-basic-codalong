package com.example.springdbbasiccodalong.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
@Getter @Setter
public class Address {
    @Id
    String id;

    String name;

    @ManyToOne
    @JoinColumn(name = "person_id")
    Person person;

    public Address(String name, Person person) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.person = person;
    }

    protected Address() {}
}
